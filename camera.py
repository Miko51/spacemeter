import numpy as np
import math
import matplotlib.pyplot as plt
import time
import cv2
import main
import ek
import os

def olc(x1,y1,x2,y2):
    alpha=math.atan2(y2-y1,x2-x1)
    return int(alpha * 180/math.pi)

def grafik(egim,zaman):
    x=[]
    y=[]
    for i in range(len(egim)):
        try:
            k=x.index(zaman[i])
            k2=y.index(egim[i])
        except ValueError:
                x.append(zaman[i])
                y.append(egim[i])
    plt.plot(x,y,"-")
    plt.grid(True)
    plt.show()

def camera(path=0):
    global cap
    cap=cv2.VideoCapture(path)
    global start_time
    start_time=time.time()
    global time2
    time2=[]
    global egim
    egim=[]
    global nokta
    nokta=[]

def resim():
            read, resim = cap.read()
            if str(type(resim))=="<class 'NoneType'>":
                cap.release()
                grafik(egim,time2)
            else :
                timer = time.gmtime(time.time() - start_time)
                time2.append(timer.tm_sec)
                gray1 = cv2.cvtColor(resim, cv2.COLOR_BGR2GRAY)
                _, thres = cv2.threshold(gray1, 127, 255, cv2.THRESH_BINARY)
                img_blur = cv2.medianBlur(thres, 11)
                circles = cv2.HoughCircles(img_blur, cv2.HOUGH_GRADIENT, 1, resim.shape[0] / 2, param1=200, param2=10,
                                           minRadius=20, maxRadius=130)
                xk = 0
                yk = 0
                if circles is not None:
                    circles = np.uint16(np.around(circles))
                    for (x, y, r) in circles[0, :]:
                        cv2.circle(resim, (x, y), r, (0, 255, 0), 4)
                        cv2.rectangle(resim, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
                        xk = x
                        yk = y

                deger = olc(0, 0, xk, yk)
                egim.append(deger)
                nokta.append(xk)
                nokta.append(yk)
                cv2.line(resim, (0, 0), (xk, yk), (0, 0, 255), 1)
                cv2.putText(resim, str(deger), (xk, yk), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0), 2)
                return resim
def pause():
    obje=ek.Pencere()
    obje.kayit(nokta)
    cap.release()
