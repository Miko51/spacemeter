from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import sys
import os
import shutil
import main
import time

class Pencere(QMainWindow):
    def __init__(self):
        self.say=0
        self.durum=1
        super().__init__()
        self.setUI()
    def setUI(self):
        tb = self.menuBar()
        file=tb.addMenu("Dosya")
        new=QAction("Yeni Proje",self)
        self.proje_list=QAction("Projeler",self)
        duzen=QAction("Sil",self)
        file.addAction(new)
        file.addAction(self.proje_list)
        file.addAction(duzen)
        file.triggered[QAction].connect(self.menu)
        self.show()

    def menu(self,q):
        if(q.text()=="Yeni Proje"):
            if(self.say==0):
                self.say = 1
                self.dialog=QDialog()
                label=QLabel("Proje Adı Girin")
                self.line=QLineEdit()
                self.text=QTextEdit()
                button=QPushButton("Olustur")
                label2=QLabel("Proje Hakkında bilgiler")
                widget=QWidget()
                v_box=QVBoxLayout(self.dialog)
                v_box.addWidget(label)
                v_box.addWidget(self.line)
                v_box.addWidget(label2)
                v_box.addWidget(self.text)
                v_box.addWidget(button)
                button.clicked.connect(self.olustur)
                self.dialog.exec()
            if (self.say == 1):
                pass
        if(q.text()=="Projeler"):
            self.dialog2=QDialog()
            self.listem = QListWidget()
            vbox=QVBoxLayout(self.dialog2)
            vbox.addWidget(self.listem)
            self.listem.addItems(i for i in os.listdir("proje/"))
            self.listem.itemDoubleClicked.connect(self.sec)
            self.dialog2.exec()

        if (q.text() == "Sil"):
            self.dialog3 = QDialog()
            self.listem = QListWidget()
            vbox = QVBoxLayout(self.dialog3)
            vbox.addWidget(self.listem)
            self.listem.addItems(i for i in os.listdir("proje/"))
            self.listem.itemDoubleClicked.connect(self.sil)
            self.dialog3.exec()
    def sil(self,q):
        box = QMessageBox()
        box.setIcon(QMessageBox.Question)
        box.setWindowTitle('Uyarı!')
        box.setText('Silmek istediğine emin misin?')
        box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        buttonY = box.button(QMessageBox.Yes)
        buttonY.setText('Evet')
        buttonN = box.button(QMessageBox.No)
        buttonN.setText('Iptal')
        box.exec_()
        if box.clickedButton() == buttonY:
            shutil.rmtree("proje/"+q.text())
            self.dialog3.close()
    def sec(self,q):
        tb = self.addToolBar("File")
        start = QAction(QIcon("/home/yeni/moon/img/start.png"), "ölçüm yapmaya başla", self)
        tb.addAction(start)
        tb.actionTriggered[QAction].connect(self.start)
        self.takvim=QCalendarWidget()
        self.proje_ad=q.text()
        self.setWindowTitle(self.proje_ad)
        self.dialog2.close()
        self.wid = QWidget()
        label = QLabel("Proje Hakkında")
        bilgi =open("proje/"+self.proje_ad+"/"+"information","r+")
        text=bilgi.read()
        os.chdir("/home/yeni/moon/proje/"+self.proje_ad+"/")
        label2=QLabel(text)
        label2.setWordWrap(True)
        hbox=QHBoxLayout()
        vbox = QVBoxLayout()
        vbox.addWidget(label)
        vbox.addWidget(label2)
        vbox.addStretch()
        hbox.addLayout(vbox)
        hbox.addWidget(self.takvim)
        self.wid.setLayout(hbox)
        self.takvim.clicked.connect(self.goster)
        self.setCentralWidget(self.wid)

    def start(self):
        x=main.Pencere(self)
        directory=time.strftime("%a %m %Y")
        if not(os.path.isdir("/home/yeni/moon/proje/"+self.proje_ad+"/"+directory)):
            os.mkdir("proje/"+self.proje_ad+"/"+directory)
        x.setWindowTitle(self.proje_ad)
        self.path="/home/yeni/moon/proje/"+self.proje_ad+"/"+directory+"/"
        os.chdir(self.path)
        x.show()

    def kayit(self,veri):
        file = open(time.strftime("%X"), "w")
        for i in veri:
            file.write(str(i)+",")

        file.close()

    def goster(self,q):
        date=q.toString()
        liste=os.listdir("proje/"+self.proje_ad+"/")
        if date in liste:
            pass
        else:
            uyari = QMessageBox()
            uyari.setText("Herhangi bir kayıt bulunamadı")
            uyari.setWindowTitle("Uyarı!")
            uyari.setIcon(QMessageBox.Warning)
            uyari.exec()

    def olustur(self):
        self.say=0
        projeler=os.listdir("proje/")
        if(self.line.text() in projeler):
            uyari=QMessageBox()
            uyari.setText("Böyle bir proje var")
            uyari.setWindowTitle("Uyarı!")
            uyari.setIcon(QMessageBox.Warning)
            uyari.exec()
        else:
            os.mkdir("proje/"+self.line.text())
            file=open("proje/"+self.line.text()+"/"+"information","w")
            file.write(self.text.toPlainText())
            self.dialog.close()






if __name__=="__main__":
    app=QApplication(sys.argv)
    pencere=Pencere()
    sys.exit(app.exec())
