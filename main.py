from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
import sys
import video
import camera
import time

class Pencere(QMainWindow):
    def __init__(self,parent=None):
        super(Pencere,self).__init__(parent=parent)
        self.setUI()
    def setUI(self):
        self.bar=QToolBar(self)
        self.addToolBar(Qt.BottomToolBarArea,self.bar)
        self.bar2=QToolBar(self)
        self.addToolBar(self.bar2)
        self.bar2.addAction(QAction(QIcon("/home/yeni/moon/img/videoicon.png"),"video",self))
        self.bar2.addAction(QAction(QIcon("/home/yeni/moon/img/camera.png"),"camera",self))
        self.bar2.actionTriggered.connect(self.tool)
        self.widget=QWidget()
        self.box=QVBoxLayout(self.widget)
        self.label = QLabel()
        self.label.setAlignment(Qt.AlignCenter)
        self.box.addWidget(self.label)
        self.widget.setLayout(self.box)
        self.setCentralWidget(self.widget)
        self.show()

    def tool(self,name):
        if name.text()=="video":
            fname = QFileDialog.getOpenFileName(self, 'Open file','c:\\', "Image files (*)")
            if fname[0]:
                selected=fname[0]
                video.camera(selected)
                self.action="image"
                self.start()
            else:
                pass

        if name.text()=="camera":
                self.pause_action=QAction(QIcon("/home/yeni/moon/img/pause.png"),"stop",self)
                self.bar2.addAction(self.pause_action)
                camera.camera()
                self.action="camera"
                self.pause_action.triggered.connect(self.pause)
                self.start()

    def pause(self):
        camera.pause()
    def start(self):
        self.timer=QTimer()
        self.timer.timeout.connect(lambda :self.resimfunc())
        self.timer.start(0)

    def resimfunc(self):
        if self.action=="image":
            resim=video.resim()
        if self.action=="camera":
            resim=camera.resim()
        if str(type(resim))=="<class 'NoneType'>":
                    self.timer.stop()
        else:
            height, width, channel = resim.shape
            bytesPerLine = 3 * width
            qImg = QImage(resim.data, width, height, bytesPerLine, QImage.Format_RGB888).rgbSwapped()
            self.im = QPixmap(qImg)
            self.label.setAlignment(Qt.AlignCenter)
            self.label.setPixmap(self.im)


if __name__=="__main__":
    app=QApplication(sys.argv)
    pencere=Pencere()
    sys.exit(app.exec())
